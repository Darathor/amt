<?php
/** @var array $availableLanguages */
?>
<form class="form-horizontal" action="" method="post">
	<?php foreach ($availableLanguages as $code => $label): ?>
		<button type="submit" name="LCID" value="<?php echo $code; ?>"><?php echo $label; ?></button>
	<?php endforeach; ?>
</form>