<?php
// Global variables.
/** @var $i18n \Darathor\Core\I18n */
$i18n = $this->i18n;
/** @var $errors array */
/** @var $warnings array */
/** @var $timezones array */
/** @var $form array */
/** @var $themes array */

if (count($errors))
{
	echo '<p class="lead">', $i18n->trans('installer_error_requirements'), '</p>';
	foreach ($errors as $error)
	{
		echo '<div class="alert alert-error">' . $error . '</div>';
	}
	foreach ($warnings as $warning)
	{
		echo '<div class="alert alert-warning">' . $warning . '</div>';
	}
}
else
{
	foreach ($warnings as $warning)
	{
		echo '<div class="alert alert-warning">' . $warning . '</div>';
	}
	if (isset($formErrors))
	{
		var_dump($formErrors);
		echo '<div class="alert alert-error">', $i18n->trans('installer_error_in_form'), '</div>';
	}
	if (isset($databaseErrors))
	{
		echo '<div class="alert alert-error">' . $databaseErrors . '</div>';
	}
	if (isset($twitterErrors))
	{
		echo '<div class="alert alert-error">' . $twitterErrors . '</div>';
	}
?>

<p class="lead"><?php echo $i18n->trans('installer_step1_description', ['ucf']); ?></p>

<form class="form-horizontal" action="" method="post">
	<input type="hidden" name="installer" value="1">
	<fieldset>
		<legend><?php echo $i18n->trans('installer_twitter_account', ['ucf']); ?></legend>
		<div class="control-group<?php if (isset($formErrors['twitterUsername'])) { echo ' error'; } ?>">
			<label class="control-label" for="twitterUsername"><?php echo $i18n->trans('installer_twitter_username', ['ucf']); ?></label>
			<div class="controls">
				<div class="input-prepend">
					<span class="add-on">@</span>
					<input id="twitterUsername" name="twitterUsername" type="text" placeholder="darathor" value="<?php echo htmlentities($form['twitterUsername']); ?>">
				</div>
				<?php if (isset($formErrors['twitterUsername'])) { echo '<div class="help-block">'.$formErrors['twitterUsername'].'</div>'; } ?>
			</div>
		</div>
	</fieldset>

	<fieldset>
		<legend><?php echo $i18n->trans('installer_twitter_app_credentials', ['ucf']); ?></legend>
		<div class="help-block">
			<?php echo $i18n->trans('installer_twitter_app_credentials_description', ['ucf']); ?>
		</div>
		<div class="control-group<?php if (isset($formErrors['consumerKey']) || isset($twitterErrors)) { echo ' error'; } ?>">
			<label class="control-label" for="consumerKey"><?php echo $i18n->trans('installer_twitter_consumer_key', ['ucf']); ?></label>
			<div class="controls">
				<input type="text" id="consumerKey" name="consumerKey" value="<?php echo htmlentities($form['consumerKey']); ?>">
				<?php if (isset($formErrors['consumerKey'])) { echo '<div class="help-block">'.$formErrors['consumerKey'].'</div>'; } ?>
			</div>
		</div>
		<div class="control-group<?php if (isset($formErrors['consumerSecret']) || isset($twitterErrors)) { echo ' error'; } ?>">
			<label class="control-label" for="consumerSecret"><?php echo $i18n->trans('installer_twitter_consumer_secret', ['ucf']); ?></label>
			<div class="controls">
				<input type="text" id="consumerSecret" name="consumerSecret" value="<?php echo htmlentities($form['consumerSecret']); ?>">
				<?php if (isset($formErrors['consumerSecret'])) { echo '<div class="help-block">'.$formErrors['consumerSecret'].'</div>'; } ?>
			</div>
		</div>
		<div class="control-group<?php if (isset($formErrors['oauthToken']) || isset($twitterErrors)) { echo ' error'; } ?>">
			<label class="control-label" for="oauthToken"><?php echo $i18n->trans('installer_twitter_consumer_twitter_oauth_token', ['ucf']); ?></label>
			<div class="controls">
				<input type="text" id="oauthToken" name="oauthToken" value="<?php echo htmlentities($form['oauthToken']); ?>">
				<?php if (isset($formErrors['oauthToken'])) { echo '<div class="help-block">'.$formErrors['oauthToken'].'</div>'; } ?>
			</div>
		</div>
		<div class="control-group<?php if (isset($formErrors['oauthSecret']) || isset($twitterErrors)) { echo ' error'; } ?>">
			<label class="control-label" for="oauthSecret"><?php echo $i18n->trans('installer_twitter_consumer_twitter_oauth_secret', ['ucf']); ?></label>
			<div class="controls">
				<input type="text" id="oauthSecret" name="oauthSecret" value="<?php echo htmlentities($form['oauthSecret']); ?>">
				<?php if (isset($formErrors['oauthSecret'])) { echo '<div class="help-block">'.$formErrors['oauthSecret'].'</div>'; } ?>
			</div>
		</div>
	</fieldset>

	<fieldset>
		<legend><?php echo $i18n->trans('installer_server', ['ucf']); ?></legend>
		<div class="control-group<?php if (isset($formErrors['baseUrl'])) { echo ' error'; } ?>">
			<label class="control-label" for="baseUrl"><?php echo $i18n->trans('installer_full_url', ['ucf']); ?></label>
			<div class="controls">
				<input type="text" id="baseUrl" name="baseUrl" value="<?php echo htmlentities($form['baseUrl']); ?>">
				<?php if (isset($formErrors['baseUrl'])) { echo '<div class="help-block">'.$formErrors['baseUrl'].'</div>'; } ?>
				<div class="help-block"><?php echo $i18n->trans('installer_full_url_help'); ?></div>
			</div>
		</div>
		<div class="control-group<?php if (isset($formErrors['timezone'])) { echo ' error'; } ?>">
			<label class="control-label" for="timezone"><?php echo $i18n->trans('installer_time_zone', ['ucf']); ?></label>
			<div class="controls">
				<select id="timezone" name="timezone">
					<?php foreach ($timezones as $tz): ?>
					<option value="<?php echo $tz; ?>"<?php if ($form['timezone'] == $tz) { echo ' selected="selected"'; } ?>><?php echo $tz; ?></option>
					<?php endforeach; ?>
				</select>
				<?php if (isset($formErrors['timezone'])) { echo '<div class="help-block">'.$formErrors['timezone'].'</div>'; } ?>
			</div>
		</div>
		<div class="control-group<?php if (isset($formErrors['cronKey'])) { echo ' error'; } ?>">
			<label class="control-label" for="cronKey"><?php echo $i18n->trans('installer_cron_key', ['ucf']); ?></label>
			<div class="controls">
				<input type="text" id="cronKey" name="cronKey" value="<?php echo htmlentities($form['cronKey']); ?>">
				<?php if (isset($formErrors['cronKey'])) { echo '<div class="help-block">'.$formErrors['cronKey'].'</div>'; } ?>
				<div class="help-block"><?php echo $i18n->trans('installer_cron_key_help', ['ucf']); ?></div>
			</div>
		</div>
	</fieldset>

	<fieldset>
		<legend><?php echo $i18n->trans('installer_database', ['ucf']); ?></legend>
		<div class="control-group<?php if (isset($formErrors['databaseHost']) || isset($databaseErrors)) { echo ' error'; } ?>">
			<label class="control-label" for="databaseHost"><?php echo $i18n->trans('installer_database_host', ['ucf']); ?></label>
			<div class="controls">
				<input type="text" id="databaseHost" name="databaseHost" value="<?php echo htmlentities($form['databaseHost']); ?>">
				<?php if (isset($formErrors['databaseHost'])) { echo '<div class="help-block">'.$formErrors['databaseHost'].'</div>'; } ?>
				<div class="help-block"><?php echo $i18n->trans('installer_database_host_help', ['ucf']); ?></div>
			</div>
		</div>
		<div class="control-group<?php if (isset($formErrors['databaseDatabase']) || isset($databaseErrors)) { echo ' error'; } ?>">
			<label class="control-label" for="databaseDatabase"><?php echo $i18n->trans('installer_database_name', ['ucf']); ?></label>
			<div class="controls">
				<input type="text" id="databaseDatabase" name="databaseDatabase" value="<?php echo htmlentities($form['databaseDatabase']); ?>">
				<?php if (isset($formErrors['databaseDatabase'])) { echo '<div class="help-block">'.$formErrors['databaseDatabase'].'</div>'; } ?>
			</div>
		</div>
		<div class="control-group<?php if (isset($formErrors['databaseUsername']) || isset($databaseErrors)) { echo ' error'; } ?>">
			<label class="control-label" for="databaseUsername"><?php echo $i18n->trans('installer_database_username', ['ucf']); ?></label>
			<div class="controls">
				<input type="text" id="databaseUsername" name="databaseUsername" value="<?php echo htmlentities($form['databaseUsername']); ?>">
				<?php if (isset($formErrors['databaseUsername'])) { echo '<div class="help-block">'.$formErrors['databaseUsername'].'</div>'; } ?>
			</div>
		</div>
		<div class="control-group<?php if (isset($formErrors['databasePassword']) || isset($databaseErrors)) { echo ' error'; } ?>">
			<label class="control-label" for="databasePassword"><?php echo $i18n->trans('installer_database_password', ['ucf']); ?></label>
			<div class="controls">
				<input type="password" id="databasePassword" name="databasePassword" value="<?php echo htmlentities($form['databasePassword']); ?>">
				<?php if (isset($formErrors['databasePassword'])) { echo '<div class="help-block">'.$formErrors['databasePassword'].'</div>'; } ?>
			</div>
		</div>
		<div class="control-group<?php if (isset($formErrors['databasePrefix']) || isset($databaseErrors)) { echo ' error'; } ?>">
			<label class="control-label" for="databasePrefix"><?php echo $i18n->trans('installer_database_table_prefix', ['ucf']); ?></label>
			<div class="controls">
				<input type="text" id="databasePrefix" name="databasePrefix" value="<?php echo htmlentities($form['databasePrefix']); ?>">
				<?php if (isset($formErrors['databasePrefix'])) { echo '<div class="help-block">'.$formErrors['databasePrefix'].'</div>'; } ?>
				<div class="help-block"><?php echo $i18n->trans('installer_database_table_prefix_help', ['ucf']); ?></div>
			</div>
		</div>
	</fieldset>

	<fieldset>
		<legend><?php echo $i18n->trans('installer_display', ['ucf']); ?></legend>
		<div class="control-group">
			<label class="control-label"><?php echo $i18n->trans('installer_language', ['ucf']); ?></label>
			<div class="controls">
				<input type="hidden" name="LCID" value="<?php echo AMT_LCID; ?>" />
				<?php echo $i18n->trans('_name', ['ucf']); ?>
			</div>
		</div>
		<div class="control-group<?php if (isset($formErrors['theme'])) { echo ' error'; } ?>">
			<label class="control-label" for="theme"><?php echo $i18n->trans('installer_theme', ['ucf']); ?></label>
			<div class="controls">
				<select id="theme" name="theme">
					<?php foreach ($themes as $theme): ?>
						<option value="<?php echo $theme['code']; ?>"<?php if ($form['theme'] == $theme['code']) { echo ' selected="selected"'; } ?>><?php echo $theme['label']; ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
	</fieldset>

	<div class="control-group">
		<div class="controls">
			<button type="submit" class="btn btn-primary btn-large"><?php echo $i18n->trans('installer_save_and_install', ['ucf']); ?> &raquo;</button>
		</div>
	</div>
</form>

<?php
}
?>