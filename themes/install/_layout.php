<?php
// Global variables.
/** @var $i18n \Darathor\Core\I18n */
$i18n = $this->i18n;
/** @var $content string */
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $i18n ? $i18n->trans('application_name') : 'AMT'; ?> <?php echo $i18n ? $i18n->trans('installer_title') : 'Installer'; ?></title>
	<link rel="icon" type="image/png" href="favicon.png" />
	<style type="text/css">
		body {
			color: #222;
			background: #eee;
			margin: 25px 10%;
			font-family: Arial, serif;
		}

		fieldset {
			border: 1px dashed #aaa;
			background: #f9f9f9;
			margin-bottom: 20px;
		}

		legend {
			font-size: 1.2em;
			font-weight: bold;
		}

		.help-block {
			font-size: 0.9em;
			color: #555;
		}

		.control-group:not(:last-child) {
			margin-bottom: 10px;
		}
		.control-group.error {
			color: #a94442;
		}

		button {
			border: 2px solid #9b9;
			background: #dfd;
			color: #555;
			font-size: 1.4em;
			padding: 5px 10px;
			border-radius: 4px;
		}

		button:hover {
			border-color: #aca;
			background: #efe;
			color: #777;
		}

		@media (min-width: 767px) {
			.control-group .control-label {
				display: inline-block;
				width: 33%;
				vertical-align: top;
			}
			.control-group .controls {
				display: inline-block;
				width: 66%;
				vertical-align: top;
			}
		}

		.alert {
			border: 1px solid;
			padding: 15px;
		}
		.alert-error {
			background-color: #f2dede;
			border-color: #ebccd1;
			color: #a94442;
		}
		.alert-warning {
			background-color: #fcf8e3;
			border-color: #faebcc;
			color: #8a6d3b;
		}
	</style>
</head>
<body>

	<div class="container">
		<div class="row">

			<div class="page-header">
				<h1>
					<?php echo $i18n ? $i18n->trans('application_name') : 'AMT'; ?>
					<small><?php echo $i18n ? $i18n->trans('installer_title') : 'Installer'; ?></small>
				</h1>
			</div>
	
			<?php echo $content; ?>

		</div><!-- /.row -->
	</div><!-- /.container -->

	<div class="footer" id="footer">
		<div class="container">
			<div class="row">
				<p>	</div>
		</div><!-- /.container -->
	</div><!-- /.footer -->
</body>
</html>
