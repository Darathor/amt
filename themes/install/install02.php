<?php
// Global variables.
/** @var $i18n \Darathor\Core\I18n */
$i18n = $this->i18n;
/** @var $archiverOutput string */
?>

<p class="lead">
	<?php echo $i18n->trans('installer_install_success', ['ucf']); ?>
</p>

<p><?php echo $i18n->trans('installer_next_step', ['ucf', 'lab']); ?></p>

<ul>
	<li><?php echo $i18n->trans('installer_next_step_1', ['ucf']); ?></li>
	<li><?php echo $i18n->trans('installer_next_step_2', ['ucf']); ?></li>
	<li><?php echo $i18n->trans('installer_next_step_3', ['ucf']); ?></li>
</ul>

<p><a href="" class="btn btn-primary btn-large"><?php echo $i18n->trans('installer_see_tweets', ['ucf']); ?> &raquo;</a></p>

<h3><?php echo $i18n->trans('installer_archiver_output', ['ucf']); ?></h3>
<p><?php echo $i18n->trans('installer_archiver_output_description', ['ucf']); ?></p>
<pre><?php echo $archiverOutput; ?></pre>