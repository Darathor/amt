<?php
// Tested only on PHP 5.5.
if (version_compare(PHP_VERSION, '5.5.0') < 0)
{
	exit('AMT requires PHP 5.5.0 or higher. Your server is running PHP ' . PHP_VERSION . '.');
}

// AMT requires 64-bit system.
if (PHP_INT_SIZE < 8)
{
	exit('AMT 64-bit system.');
}

// Initialization.
define('ROOT_DIR', dirname(__DIR__));
if (!file_exists(ROOT_DIR . '/config/config.php'))
{
	die('Missing config/config.php file. Copy config/config.example.php to config/config.php and customize the settings' . PHP_EOL);
}

// Run.
/** @var \Darathor\Core\Core $core */
require_once ROOT_DIR . '/includes.php';

$amt = new \Darathor\Amt\App($core);
$amt->downloadEntities(false, true);