<?php
require_once ROOT_DIR . '/core/Core.php';
require_once ROOT_DIR . '/amt/App.php';
// Twitter API lib.
require_once ROOT_DIR . '/vendor/tijsverkoyen/TwitterOAuth/Twitter.php';
require_once ROOT_DIR . '/vendor/tijsverkoyen/TwitterOAuth/Exception.php';

if (file_exists(ROOT_DIR . '/config/config.php'))
{
	/** @var array $config */
	require_once ROOT_DIR . '/config/config.php';
	if (!isset($config['display']['theme']))
	{
		$config['display']['theme'] = 'default';
	}
	if (!isset($config['display']['LCID']))
	{
		$config['display']['LCID'] = 'fr_FR';
	}
	if (!isset($config['display']['timezone']))
	{
		$config['display']['timezone'] = 'Europe/Paris';
	}
	date_default_timezone_set($config['display']['timezone']);
	$core = new \Darathor\Core\Core($config);
}
else
{
	$core = new \Darathor\Core\Core([
		'display' => [
			'LCID' => defined('AMT_LCID') ? AMT_LCID : 'fr_FR'
		]
	]);
}
