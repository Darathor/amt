<?php
namespace Darathor\Core;

require_once ROOT_DIR . '/vendor/proximis/Change/Stdlib/FloatUtils.php';
require_once ROOT_DIR . '/vendor/proximis/Change/Stdlib/StringUtils.php';

spl_autoload_register(function($name)
{
	if (strpos($name, 'Darathor\\Core\\') === 0)
	{
		$filename = ROOT_DIR . '/' . str_replace('Darathor/Core/', 'core/', str_replace("\\", '/', $name)) . '.php';
		require_once $filename;
		return;
	}
});

/**
 * @name \Darathor\Core\Core
 */
class Core
{
	/**
	 * @var \Darathor\Core\Configuration
	 */
	protected $configuration;

	/**
	 * @var \Darathor\Core\I18n
	 */
	protected $i18n;

	/**
	 * @param array $config
	 */
	public function __construct($config)
	{
		$this->configuration = new \Darathor\Core\Configuration($config);
	}

	/**
	 * @return \Darathor\Core\I18n
	 * @throws \InvalidArgumentException
	 */
	public function getI18n()
	{
		if ($this->i18n === null)
		{
			$configuration = $this->getConfiguration();
			$this->i18n = new \Darathor\Core\I18n($configuration->get('display', 'LCID'), $configuration->get('display', 'timezone'));
		}
		return $this->i18n;
	}

	/**
	 * @return \Darathor\Core\Configuration
	 */
	public function getConfiguration()
	{
		return $this->configuration;
	}
}