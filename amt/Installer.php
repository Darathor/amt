<?php
namespace Darathor\Amt;

/**
 * Installer
 */
class Installer
{
	/**
	 * @var \Darathor\Core\Core
	 */
	protected $core;

	protected $root;
	protected $directory;
	protected $view;
	protected $data;
	protected $model;
	protected $twitter;

	/**
	 * Constructor
	 *
	 * @param \Darathor\Core\Core $core
	 * @param string $directory The directory to install in.
	 * @throws \LogicException
	 */
	public function __construct($core, $directory)
	{
		$this->core = $core;
		$this->root = $directory;
		$this->directory = $directory . '/config';
		$this->view = new View($directory . '/themes/install', $core->getI18n());
		$this->data = [];
	}

	/**
	 * Run the installer
	 * @throws \Exception
	 */
	public function run()
	{
		if (!defined('AMT_LCID'))
		{
			$this->step0();
		}
		else
		{
			$this->step1();
		}
	}

	/**
	 * Step 0: verify requirements and display language selection from.
	 * @throws \Exception
	 */
	public function step0()
	{
		$availableLanguages = [];
		foreach (glob($this->root . '/i18n/*.php') as $path)
		{
			$languageData = $this->getLanguageData($path);
			$availableLanguages[$languageData[0]] = $languageData[1];
		}
		$this->data['availableLanguages'] = $availableLanguages;
		$this->data['content'] = $this->view->render('install00.php', $this->data);
		$this->view->render('_layout.php', $this->data, true);
	}

	/**
	 * @param string $path
	 * @return array
	 * @throws \LogicException
	 */
	protected function getLanguageData($path)
	{
		$i18n = null;
		include $path;
		if (!is_array($i18n) || !isset($i18n['_LCID'], $i18n['_name']))
		{
			throw new \LogicException('Invalid language file: ' . $path);
		}
		return [$i18n['_LCID'], $i18n['_name']];
	}

	/**
	 * @return array
	 * @throws \Exception
	 */
	protected function getThemes()
	{
		$themes = [];
		foreach (glob($this->root . '/themes/*') as $path)
		{
			$filePath = $path . '/theme.json';
			if (is_readable($filePath))
			{
				$themes[] = json_decode(file_get_contents($path . '/theme.json'), true);
			}
		}
		return $themes;
	}

	/**
	 * Step 1: verify requirements and display config form.
	 * @throws \Exception
	 */
	public function step1()
	{
		$problems = $this->verifyRequirements();
		$this->data['errors'] = $problems['errors'];
		$this->data['warnings'] = $problems['warnings'];

		$this->data['timezones'] = $this->getTimeZones();
		$this->data['themes'] = $this->getThemes();

		// form data
		$this->data['form']['twitterUsername'] = (isset($_POST['twitterUsername']) && $_POST['twitterUsername']) ? $_POST['twitterUsername'] : '';
		$this->data['form']['consumerKey'] = (isset($_POST['consumerKey']) && $_POST['consumerKey']) ? $_POST['consumerKey'] : '';
		$this->data['form']['consumerSecret'] = (isset($_POST['consumerSecret']) && $_POST['consumerSecret']) ? $_POST['consumerSecret'] : '';
		$this->data['form']['oauthToken'] = (isset($_POST['oauthToken']) && $_POST['oauthToken']) ? $_POST['oauthToken'] : '';
		$this->data['form']['oauthSecret'] = (isset($_POST['oauthSecret']) && $_POST['oauthSecret']) ? $_POST['oauthSecret'] : '';
		$this->data['form']['baseUrl'] = (isset($_POST['baseUrl']) && $_POST['baseUrl']) ? $_POST['baseUrl'] : $this->guessBaseUrl();
		$this->data['form']['timezone'] = (isset($_POST['timezone']) && $_POST['timezone']) ? $_POST['timezone'] : 'Europe/Paris';
		$this->data['form']['cronKey'] = (isset($_POST['cronKey']) && $_POST['cronKey']) ? $_POST['cronKey'] : '';
		$this->data['form']['databaseHost'] = (isset($_POST['databaseHost']) && $_POST['databaseHost']) ? $_POST['databaseHost'] : 'localhost';
		$this->data['form']['databaseDatabase'] = (isset($_POST['databaseDatabase']) && $_POST['databaseDatabase']) ? $_POST['databaseDatabase'] : '';
		$this->data['form']['databaseUsername'] = (isset($_POST['databaseUsername']) && $_POST['databaseUsername']) ? $_POST['databaseUsername'] : '';
		$this->data['form']['databasePassword'] = (isset($_POST['databasePassword']) && $_POST['databasePassword']) ? $_POST['databasePassword'] : '';
		$this->data['form']['databasePrefix'] = (isset($_POST['databasePrefix']) && $_POST['databasePrefix']) ? $_POST['databasePrefix'] : 'amt_';
		$this->data['form']['theme'] = (isset($_POST['theme']) && $_POST['theme']) ? $_POST['theme'] : 'default';

		// form submission
		if (isset($_POST['installer']))
		{
			$installProblem = false;

			// validate form data
			$formErrors = $this->validateFormData($this->data['form']);
			if ($formErrors !== false)
			{
				$installProblem = true;
				$this->data['formErrors'] = $formErrors;
			}

			// test database connection
			if (!$installProblem)
			{
				try
				{
					$dsn =
						'mysql:host=' . $this->data['form']['databaseHost'] . ';dbname=' . $this->data['form']['databaseDatabase'] . ';charset=utf8mb4';
					$user = $this->data['form']['databaseUsername'];
					$pass = $this->data['form']['databasePassword'];
					$db = new \PDO($dsn, $user, $pass);
					$this->model = new \Darathor\Amt\Model($db, $this->data['form']['databasePrefix'], 0);
				}
				catch (\Exception $e)
				{
					$installProblem = true;
					$this->data['databaseErrors'] = 'There was a problem connecting to your database: ' . $e->getMessage();
				}
			}

			// test twitter connection
			if (!$installProblem)
			{
				$this->twitter = new \TijsVerkoyen\Twitter\Twitter($this->data['form']['consumerKey'], $this->data['form']['consumerSecret']);
				$this->twitter->setOAuthToken($this->data['form']['oauthToken']);
				$this->twitter->setOAuthTokenSecret($this->data['form']['oauthSecret']);

				try
				{
					$userData = $this->twitter->usersShow(null, $this->data['form']['twitterUsername']);
					$this->data['form']['twitterId'] = $userData['id'];
					$this->data['form']['twitterScreenName'] = $userData['screen_name'];
				}
				catch (\Exception $e)
				{
					$installProblem = true;
					$this->data['twitterErrors'] = 'There was a problem connecting to twitter: ' . $e->getMessage();
				}
			}

			// use model to install database, if it fails, return form with errors
			if (!$installProblem)
			{
				try
				{
					$this->model->install();
				}
				catch (\Exception $e)
				{
					$installProblem = true;
					$this->data['databaseErrors'] = 'There was a problem while installing the database tables: ' . $e->getMessage();
				}

				// make sure
				if (!$this->model->isInstalled())
				{
					$installProblem = true;
					$this->data['databaseErrors'] = 'The database tables were not installed for some unknown reason.';
				}
			}

			// write the config.php file to disk, it it fails, return form with errors
			if (!$installProblem)
			{
				$written = $this->writeConfig($this->directory, $this->data['form']);
				if (!$written)
				{
					$installProblem = true;
					$this->data['configError'] =
						'The config.php file could not be written. Please check the permissions on this directory: ' . $this->directory;
				}
			}

			// after all this, if there were no problems, then redirect to step 2
			if (!$installProblem)
			{
				$this->step2();
				return;
			}
		}

		$this->data['content'] = $this->view->render('install01.php', $this->data);
		$this->view->render('_layout.php', $this->data, true);
	}

	/**
	 * Step 2: archive and display
	 * @throws \LogicException
	 */
	public function step2()
	{
		// run the archiver
		$archiver = new Archiver($this->data['form']['twitterId'], $this->data['form']['twitterUsername'], $this->twitter, $this->model);
		$this->data['archiverOutput'] = $archiver->archive();

		$this->data['content'] = $this->view->render('install02.php', $this->data);
		$this->view->render('_layout.php', $this->data, true);
	}

	/**
	 * Validates form data and returns an array of errors if any, or false if all data validates
	 * @param array $data
	 * @return array|bool
	 */
	public function validateFormData($data)
	{
		$errors = [];

		// twitter username
		if (!$data['twitterUsername'] || !preg_match('/^[a-zA-Z0-9_]+$/', $data['twitterUsername']) || strlen($data['twitterUsername']) > 15)
		{
			$errors['twitterUsername'] = 'Username must only use letters, numbers, underscores, and be 15 or fewer characters in length.';
		}

		// cron key
		if (!$data['cronKey'] || !preg_match('/^[a-zA-Z0-9_]+$/', $data['cronKey']) || strlen($data['cronKey']) > 50)
		{
			$errors['cronKey'] = 'The cron key must only use letters, numbers, underscores, and be 50 or fewer characters in length.';
		}

		// timezone
		if (!date_default_timezone_set($data['timezone']))
		{
			$errors['timezone'] = 'Not a valid timezone.';
		}

		// database prefix
		if (!$data['databasePrefix'] || strlen($data['databasePrefix']) > 15 || !preg_match('/^[a-zA-Z0-9_]+$/', $data['databasePrefix']))
		{
			$errors['databasePrefix'] = 'The database prefix must only use letters, numbers, underscores, and be 15 or fewer characters in length.';
		}

		// do one last check to require all fields without a specific check
		$requiredFields = ['twitterUsername', 'consumerKey', 'consumerSecret', 'oauthToken', 'oauthSecret', 'baseUrl', 'timezone', 'cronKey',
				'databaseHost', 'databaseDatabase', 'databaseUsername', 'databasePassword', 'databasePrefix'];
		foreach ($requiredFields as $field)
		{
			if (!isset($errors[$field]) && trim($data[$field]) !== '')
			{
				$errors[$field] = 'This field is required.';
			}
		}

		return count($errors) ? $errors : false;
	}

	/**
	 * Guesses the base URL for this app
	 */
	protected function guessBaseUrl()
	{
		$pageURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') ? 'https://' : 'http://';
		if ($_SERVER['SERVER_PORT'] !== '80')
		{
			$pageURL .= $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . $_SERVER['REQUEST_URI'];
		}
		else
		{
			$pageURL .= $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		}
		$pageURL .= !\Change\Stdlib\StringUtils::endsWith($pageURL, '/') ? '/' : '';
		$pageURL .= !\Change\Stdlib\StringUtils::endsWith($pageURL, 'index.php/') ? 'index.php/' : '';
		return $pageURL;
	}

	/**
	 * Returns a list of valid timezones
	 */
	public function getTimeZones()
	{
		$regions = [
			'Africa' => \DateTimeZone::AFRICA,
			'America' => \DateTimeZone::AMERICA,
			'Antarctica' => \DateTimeZone::ANTARCTICA,
			'Asia' => \DateTimeZone::ASIA,
			'Atlantic' => \DateTimeZone::ATLANTIC,
			'Europe' => \DateTimeZone::EUROPE,
			'Indian' => \DateTimeZone::INDIAN,
			'Pacific' => \DateTimeZone::PACIFIC
		];

		$tzList = [];
		foreach ($regions as $name => $mask)
		{
			/** @noinspection SlowArrayOperationsInLoopInspection */
			$tzList = array_merge($tzList, \DateTimeZone::listIdentifiers($mask));
		}

		return $tzList;
	}

	/**
	 * Writes the config.php file
	 * @param string $directory
	 * @param array $data
	 * @return bool
	 */
	public function writeConfig($directory, $data)
	{
		$config = '<?php' . PHP_EOL;

		// timezone
		$config .= "date_default_timezone_set('" . htmlspecialchars($data['timezone'], ENT_QUOTES) . "');" . PHP_EOL;

		// define all config settings
		$config .= "define('TWITTER_USERNAME',        '" . htmlspecialchars($data['twitterUsername'], ENT_QUOTES) . "');" . PHP_EOL;
		$config .= "define('TWITTER_SCREEN_NAME',     '" . htmlspecialchars($data['twitterScreenName'], ENT_QUOTES) . "');" . PHP_EOL;
		$config .= "define('TWITTER_ID',              " . (int)$data['twitterId'] . ');' . PHP_EOL;
		$config .= "define('BASE_URL',                '" . htmlspecialchars($data['baseUrl'], ENT_QUOTES) . "');" . PHP_EOL;
		$config .= "define('TWITTER_CONSUMER_KEY',    '" . htmlspecialchars($data['consumerKey'], ENT_QUOTES) . "');" . PHP_EOL;
		$config .= "define('TWITTER_CONSUMER_SECRET', '" . htmlspecialchars($data['consumerSecret'], ENT_QUOTES) . "');" . PHP_EOL;
		$config .= "define('TWITTER_OAUTH_TOKEN',     '" . htmlspecialchars($data['oauthToken'], ENT_QUOTES) . "');" . PHP_EOL;
		$config .= "define('TWITTER_OAUTH_SECRET',    '" . htmlspecialchars($data['oauthSecret'], ENT_QUOTES) . "');" . PHP_EOL;
		$config .= "define('DB_USERNAME',             '" . htmlspecialchars($data['databaseUsername'], ENT_QUOTES) . "');" . PHP_EOL;
		$config .= "define('DB_PASSWORD',             '" . htmlspecialchars($data['databasePassword'], ENT_QUOTES) . "');" . PHP_EOL;
		$config .= "define('DB_NAME',                 '" . htmlspecialchars($data['databaseDatabase'], ENT_QUOTES) . "');" . PHP_EOL;
		$config .= "define('CRON_SECRET',             '" . htmlspecialchars($data['cronKey'], ENT_QUOTES) . "');" . PHP_EOL;
		$config .= "define('DB_TABLE_PREFIX',         '" . htmlspecialchars($data['databasePrefix'], ENT_QUOTES) . "');" . PHP_EOL;
		$config .= "define('DB_HOST',                 '" . htmlspecialchars($data['databaseHost'], ENT_QUOTES) . "');" . PHP_EOL;
		$config .= "define('AMT_LCID',                '" . htmlspecialchars(AMT_LCID, ENT_QUOTES) . "');" . PHP_EOL;
		$config .= "define('AMT_THEME',               '" . htmlspecialchars($data['theme'], ENT_QUOTES) . "');" . PHP_EOL;

		$filename = $directory . '/config.php';
		return (bool)file_put_contents($filename, $config);
	}

	/**
	 * Checks that all installation requirements are met
	 *
	 * @return array Returns an array of error messages for missing requirements.
	 */
	public function verifyRequirements()
	{
		$problems = [
			'warnings' => [],
			'errors' => []
		];

		// Requirement: PHP 5.5
		if (version_compare(PHP_VERSION, '5.5.0') < 0)
		{
			$problems['errors'][] = 'PHP 5.5.0 or greater is required. Your installed version is ' . PHP_VERSION . '.';
		}

		// Requirement: PDO
		if (!class_exists('PDO'))
		{
			$problems['errors'][] = 'The PHP Data Objects (PDO) extension is required.';
		}

		// Requirement: cURL
		if (!function_exists('curl_init'))
		{
			$problems['errors'][] = 'The PHP cURL extension is required.';
		}

		// Requirement: writable config.php
		if (!is_writable($this->directory))
		{
			$problems['errors'][] =
				'The directory for your config.php file is not writable. Please change the permissions on this directory (through SSH or your FTP client) so it writable to all users. When this installer is completed, you can change the permissions back. The directory to make writable: <code>'
				. $this->directory . '</code>';
		}

		// Optional: json_decode
		if (!function_exists('json_decode'))
		{
			$problems['warnings'][] =
				'Your version of PHP is missing the <code>json_decode()</code> function. This is included and enabled by default for PHP versions 5.2.0 and higher. This is only required if you want to import tweets from an official twitter archive download, otherwise Archive My Tweets can run without it.';
		}

		// Optional: 64-bit integers
		if (PHP_INT_SIZE !== 8)
		{
			$problems['warnings'][] =
				'Your PHP installation does not support 64 bit integers and this may cause problems for you. Support for 64 bit integers is recommended and is offered by most modern web hosts.';
		}

		return $problems;
	}
}