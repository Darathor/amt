<?php
namespace Darathor\Amt;

spl_autoload_register(function($name)
{
	if (strpos($name, 'Darathor\\Amt\\') === 0)
	{
		$filename = ROOT_DIR . '/' . str_replace('Darathor/Amt/', 'amt/', str_replace("\\", '/', $name)) . '.php';
		require_once $filename;
		return;
	}
});

/**
 * The ArchiveMyTweets application class.
 */
class App
{
	/**
	 * @var \Darathor\Core\Core
	 */
	protected $core;

	/**
	 * @var \Darathor\Amt\Model
	 */
	protected $model;

	/**
	 * @var \Darathor\Amt\View
	 */
	protected $view;

	/**
	 * @var \Darathor\Amt\Controller
	 */
	protected $controller;

	/**
	 * @var \Darathor\Amt\Router
	 */
	protected $router;

	// current version
	const VERSION = '0.1';

	/**
	 * @param \Darathor\Core\Core $core
	 * @throws \Exception
	 */
	public function __construct($core)
	{
		$this->core = $core;
		$configuration = $core->getConfiguration();
		$i18n = $core->getI18n();

		// Model.
		try
		{
			$dbConfig = $configuration->get('db');
			$dsn = 'mysql:host=' . $dbConfig['host'] . ';dbname=' . $dbConfig['database'] . ';charset=utf8mb4';
			$db = new \PDO($dsn, $dbConfig['username'], $dbConfig['password']);
			$this->model = new \Darathor\Amt\Model($db, $dbConfig['prefix'], $configuration->get('twitter', 'id'));
		}
		catch (\PDOException $e)
		{
			throw $e;
		}

		// View.
		try
		{
			$this->view = new \Darathor\Amt\View(ROOT_DIR . '/themes/' . $configuration->get('display', 'theme'), $i18n, $configuration);
		}
		catch (\Exception $e)
		{
			throw $e;
		}

		// Paginator.
		try
		{
			$this->paginator = new \Darathor\Amt\Paginator($this->view);
		}
		catch (\Exception $e)
		{
			throw $e;
		}

		// Controller
		$controllerData = [
			'config' => [
				'twitter' => $configuration->get('twitter'),
				'system' => [
					'baseUrl' => $configuration->get('system', 'baseUrl')
				],
				'display' => [
					'theme' => $configuration->get('display', 'theme')
				]
			]
		];
		$this->controller = new \Darathor\Amt\Controller($this->model, $this->view, $this->paginator, $i18n, $controllerData);
		$this->router = new \Darathor\Amt\Router($this->controller);
	}

	/**
	 * Returns the core
	 */
	public function getCore()
	{
		return $this->core;
	}

	/**
	 * Runs the web interface
	 * @param string $pathInfo For example: "/archive/2016/05/15/"
	 */
	public function run($pathInfo)
	{
		$this->router->route($pathInfo);
	}

	/**
	 * Grabs all the latest tweets and puts them into the database.
	 *
	 * @return string Returns a string with informational output.
	 */
	public function archiveTimeline()
	{
		$config = $this->core->getConfiguration();
		$authConfig = $config->get('auth');

		// create twitter instance
		$twitter = new \TijsVerkoyen\Twitter\Twitter($authConfig['consumerKey'], $authConfig['consumerSecret']);
		$twitter->setOAuthToken($authConfig['oauthToken']);
		$twitter->setOAuthTokenSecret($authConfig['oauthSecret']);

		$archiver = new \Darathor\Amt\Archiver($config->get('twitter', 'id'), $config->get('twitter', 'username'), $twitter, $this->model);
		return $archiver->archive(\Darathor\Amt\Archiver::TYPE_TIMELINE);
	}

	/**
	 * Grabs all the latest favorites and puts them into the database.
	 *
	 * @return string Returns a string with informational output.
	 */
	public function archiveFavorites()
	{
		$config = $this->core->getConfiguration();
		$authConfig = $config->get('auth');

		// create twitter instance
		$twitter = new \TijsVerkoyen\Twitter\Twitter($authConfig['consumerKey'], $authConfig['consumerSecret']);
		$twitter->setOAuthToken($authConfig['oauthToken']);
		$twitter->setOAuthTokenSecret($authConfig['oauthSecret']);

		$archiver = new \Darathor\Amt\Archiver($config->get('twitter', 'id'), $config->get('twitter', 'username'), $twitter, $this->model);
		return $archiver->archive(\Darathor\Amt\Archiver::TYPE_FAVORITES);
	}

	/**
	 * Archive a specific tweet from Twitter.
	 *
	 * @param integer $id
	 * @return string Returns a string with informational output.
	 */
	public function archiveTweet($id)
	{
		$config = $this->core->getConfiguration();
		$authConfig = $config->get('twitter', 'auth');

		// create twitter instance
		$twitter = new \TijsVerkoyen\Twitter\Twitter($authConfig['consumerKey'], $authConfig['consumerSecret']);
		$twitter->setOAuthToken($authConfig['oauthToken']);
		$twitter->setOAuthTokenSecret($authConfig['oauthSecret']);

		$archiver = new \Darathor\Amt\Archiver($config->get('twitter', 'id'), $config->get('twitter', 'username'), $twitter, $this->model);
		return $archiver->archive($id);
	}

	/**
	 * Imports tweets from the JSON files in a downloaded Twitter Archive
	 *
	 * @param string $directory The directory to look for Twitter .js files.
	 * @return string Returns a string with informational output.
	 */
	public function importJSON($directory)
	{
		$importer = new \Darathor\Amt\Importer($this->core->getConfiguration()->get('twitter', 'id'));
		return $importer->importJSON($directory, $this->model);
	}

	/**
	 * Load the missing entities in tweets.
	 *
	 * @param boolean $replace
	 * @param boolean $verbose
	 * @throws \RuntimeException
	 */
	public function downloadEntities($replace = false, $verbose = false)
	{
		$ownerId = (int)$this->core->getConfiguration()->get('twitter', 'id');
		$offset = 0;
		do
		{
			if ($verbose)
			{
				echo 'Tweets from ' . $offset . ' to ' . ($offset + 50) . ': ';
			}

			$rows = $this->model->getTweets(['own' => true, 'replies' => true, 'retweets' => true, 'favorites' => true], $offset);
			foreach ($rows as $row)
			{
				$tweet = new \Darathor\Amt\Tweet($ownerId);
				$tweet->load($row);

				$this->downloadEntitiesForTweet($tweet, $replace, $verbose);
			}

			if ($verbose)
			{
				echo PHP_EOL;
			}

			$offset += 50;
		}
		while ($rows);
	}

	/**
	 * @param Tweet $tweet
	 * @param boolean $replace
	 * @param boolean $verbose
	 * @throws \RuntimeException
	 */
	protected function downloadEntitiesForTweet(\Darathor\Amt\Tweet $tweet, $replace = false, $verbose = false)
	{
		$tweet->getAvatar()->download($replace);
		echo '.';

		foreach ($tweet->getEntities() as $entity)
		{
			$entity->download($replace);
			/** @noinspection DisconnectedForeachInstructionInspection */
			if ($verbose)
			{
				echo '.';
			}
		}

		$quoted = $tweet->getQuotedTweet();
		if ($quoted)
		{
			$this->downloadEntitiesForTweet($quoted, $replace, $verbose);
		}
	}
}