<?php
// Tested only on PHP 5.5.
if (version_compare(PHP_VERSION, '5.5.0') < 0)
{
	exit('AMT requires PHP 5.5.0 or higher. Your server is running PHP ' . PHP_VERSION . '.');
}

// AMT requires 64-bit system.
if (PHP_INT_SIZE < 8)
{
	exit('AMT requires a 64-bit system.');
}

// Run.
define('ROOT_DIR', dirname(__DIR__));
if (file_exists(ROOT_DIR . '/config/config.php'))
{
	/** @var \Darathor\Core\Core $core */
	require_once ROOT_DIR . '/includes.php';
	$amt = new \Darathor\Amt\App($core);
	$amt->run(isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '');
}
else
{
	if (isset($_POST['LCID']) && preg_match('/[a-z]{2}_[A-Z]{2}/', $_POST['LCID'], $matches))
	{
		define('AMT_LCID', $matches[0]);
	}
	/** @var \Darathor\Core\Core $core */
	require_once ROOT_DIR . '/includes.php';
	require_once ROOT_DIR . '/amt/Installer.php';
	$installer = new \Darathor\Amt\Installer($core, ROOT_DIR);
	$installer->run();
}