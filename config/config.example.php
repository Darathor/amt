<?php
$config = [
	// Twitter data.
	'twitter' => [
		'username' => '', // e.g. darathor
		'screenName' => '', // e.g. Darathor
		'id' => 0, // Your user id (it is retrieved during the install process)
		// OAuth configuration: register at http://dev.twitter.com/apps/
		'auth' => [
			'consumerKey' => '',
			'consumerSecret' => '',
			'oauthToken' => '',
			'oauthSecret' => ''
		]
	],
	// MySQL database credentials.
	'db' => [
		'host' => 'localhost', // You can add a port number like this: example.com:3306
		'username' => '',
		'password' => '',
		'database' => '',
		'prefix' => 'amt_' // A prefix added to each table name.
	],
	'system' => [
		// The URL for your installation of AMT.
		'baseUrl' => 'http://amt.vm/index.php/', // e.g. http://amwhalen.com/twitter/ (start with http:// and have a slash at the end)
		// To run a cron job a secret key is required so no one can destroy your API limit by visiting cron.php this can be anything you want.
		'cronSecret' => ''
	],
	// UI configuration.
	'display' => [
		'theme' => 'default',
		'LCID' => 'fr_FR',
		// timezone, see: http://php.net/manual/en/timezones.php
		'timezone' => 'Europe/Paris' // e.g. America/New_York
	]
];